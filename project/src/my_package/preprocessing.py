import nltk
import re
from nltk import wordnet
from nltk.stem import WordNetLemmatizer
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from autocorrect import Speller

def tokenize(text):
    """Tokenize text and return list of tokens."""
    return nltk.word_tokenize(text)

def to_lower_case(words):
    """Return list of strings in lower case letters."""
    return [w.lower() for w in words]

def remove_short_words(words):
    """Remove all strings that contain less than 3 letters and return filtered list of strings."""
    min_len = 3
    return [w for w in words if len(w) >= min_len] 

def remove_links(words):
    """Remove all strings that represent a link, hashtag or username and return filtered list of strings."""
    patterns = [r'https?:\/\/\S*', r'spr\.ly\/\S*', r'@[A-Za-z0-9_]+', r'#[A-Za-z0-9_]+']
    for pattern in patterns:
        r = re.compile(pattern)
        words = [w for w in words if not re.match(r, w)]
    return words

# Removing all non letter characters. Character "'" is not removed because it is needed for treating negations.
def remove_symbols(words):
    """Remove all strings that contain a non letter character except character ``'`` and return filtered list of strings."""
    r = re.compile("[a-z']+")
    return list(filter(r.match, words))

def remove_non_english_words(words):
    """Remove all strings that don't represent an english word and return filtered list of strings."""
    english_words = set(nltk.corpus.words.words())
    return [w for w in words if w in english_words]

def remove_stop_words(words):
    """Remove all strings that are part of ```nltk.corpus``` ```stopwords``` set and return filtered list of strings."""
    stop_words = set(stopwords.words('english'))
    stop_words.remove('no')
    stop_words.remove('not')
    return [w for w in words if not w in stop_words]

def correct_spelling(words):
    """Return list of words with corrected spelling."""
    spell = Speller(lang='en')
    return [spell(w) for w in words]

def _get_pos_tag(word):
    """Return Part-of-speech tag of given word. Only tag adjectives, nouns verbs and adverbs. Tag all other words as ```other```."""
    tag = nltk.pos_tag([word])[0][1][0].lower()
    tag_dict = {"j": wordnet.ADJ,
                "n": wordnet.NOUN,
                "v": wordnet.VERB,
                "r": wordnet.ADV}
    return tag_dict.get(tag, "other")

def lemmatize(words):
    """Determine the lemmas of all words based on their PoS tag and return list of lemmas."""
    lemmatizer = WordNetLemmatizer()
    return [lemmatizer.lemmatize(w, _get_pos_tag(w)) for w in words if _get_pos_tag(w) != "other"]

def stem(words):
    """Return list of word stems."""
    stemmer = SnowballStemmer("english")
    return [stemmer.stem(w) for w in words]

def document_preprocessing(text):
    """Preprocess document.

    Tokenize text and apply the following operations: 
    remove short words, convert words to all lower case letters,
    remove links, hashtags and usernames, remove words that contain non letter characters,
    remove non english words, correct the spelling of words,
    convert words to their root form. Join the words and return a string representing a preprocessed document.
    """
    words = tokenize(text)
    words = remove_short_words(words)
    words = to_lower_case(words)
    words = remove_links(words)
    words = remove_symbols(words)
    words = remove_non_english_words(words)
    words = remove_stop_words(words)
    words = correct_spelling(words)
    # words = lemmatize(words)
    words = stem(words)
    return ' '.join(words)
