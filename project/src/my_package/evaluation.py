import math
from prettytable import PrettyTable
from my_package.transformations import normalize_list
from sklearn.metrics import mean_squared_error

def _format_result(document, emotions, actual, predicted, rmse):
    """Format result of text analysis. 
    
    Return a string in a table form that holds the actual and predicted values of the analysis algorithm,
    and information about the root mean square error of algorithm resutls."""
    res = document + "\n"
    t = PrettyTable(["values"] + emotions)
    t.add_row(["actual"] + [round(num, 2) for num in actual])
    t.add_row(["predicted"] + [round(num, 2) for num in predicted])
    res += str(t)
    res += "\nRMSE is " + str(rmse) + ".\n\n"
    return res

def evaulate_emotion_lexicon_algorithm(term_by_emotion_df, test_document_by_emotion_df, algorithm, output_file=None):
    """Evaluate emotion lexicon analysis algorithm.
    
    Using the test_document_by_emotion_df dataframe calculate root mean square error of algorithm emotion scores.
    Term_by_document_df dataframe object represents the emotion lexicon that the algorithm uses. If output_file is provided,
    output the evaluation results in file.
    """
    if(output_file is not None):
        f = open(output_file, "w")
        f.write("EVALUATION RESULTS\n\n\n")
    rmse_sum = 0
    counter = 0
    for document, scores in test_document_by_emotion_df.iterrows():
        actual_scores = normalize_list(scores)
        emotion_to_score = algorithm(document, term_by_emotion_df)
        predicted_scores = list(emotion_to_score.values())
        rmse = math.sqrt(mean_squared_error(actual_scores, predicted_scores))
        rmse_sum += rmse
        counter += 1
        if(output_file is not None):
            f.write(_format_result(document, list(term_by_emotion_df.columns), actual_scores, predicted_scores, rmse))
    rmse_mean = rmse_sum / counter
    if(output_file is not None):
        f.write("\nMean RMSE is " + str(round(rmse_mean, 3)))
        f.close()
