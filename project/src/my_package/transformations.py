from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
import numpy as np

def create_term_by_document_tfidf(documents):
    """Convert a collection of raw documents to a dataframe of TF-IDF features."""
    tfidf_vectorizer = TfidfVectorizer(analyzer='word')
    tfidf_matrix = tfidf_vectorizer.fit_transform(documents)
    tfidf_features = tfidf_vectorizer.get_feature_names_out()
    return pd.DataFrame(data = tfidf_matrix.toarray(),index = documents,columns = tfidf_features).transpose()

def normalize_matrix_rows(matrix):
    """Return matrix with rows scaled so that the sum of values in each row equals 1."""
    row_sums = matrix.sum(axis=1)
    return matrix / row_sums[:, np.newaxis]

def normalize_list(values):
    """Scale list values so that the sum of values equals 1 and return list of scaled values."""
    return list(np.array(values) / sum(values))