from setuptools import find_packages, setup

setup(
    name='my_package',
    version='0.1.0',
    packages=find_packages(include=['my_package']),
    install_requires=['pandas', 'numpy', 'prettytable', 'sklearn', 'nltk', 'autocorrect']
)