EVALUATION RESULTS


Experience the essence of the Galaxy DNA, but with a totally different attitude. Introducing #GalaxyA2017.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.03    |    0.69    |    0.01   |    0.26   |
| predicted |     0.33    |    0.14    |    0.19    |    0.11   |    0.23   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.27373992591884844.

The #GearFit2, #GearS2 and #GearS3 now seamlessly sync to any of the Under Armour Connected Fitness apps.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.02    |    0.72    |    0.0    |    0.22   |
| predicted |     0.29    |    0.21    |    0.24    |    0.11   |    0.16   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.26425499327978536.

More music to your ears. Spotify is now available for the #GearFit2, #GearS2 and #GearS3.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.04    |    0.02    |    0.73    |    0.01   |    0.19   |
| predicted |     0.17    |    0.16    |    0.26    |    0.25   |    0.16   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.25367239702450095.

Introducing Samsung LEVEL Box Slim, a portable wireless speaker with powerful sound. http://spr.ly/61808P1nE
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.02    |    0.62    |    0.0    |    0.35   |
| predicted |     0.28    |    0.27    |    0.2     |    0.12   |    0.13   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.27287252149123026.

Capture better selfies with #GalaxyA2017.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.02    |    0.58    |    0.01   |    0.38   |
| predicted |     0.18    |    0.14    |    0.38    |    0.05   |    0.26   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.13998761284273734.

Play with water. #GalaxyA2017 #IP68
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.07    |    0.65    |    0.01   |    0.25   |
| predicted |     0.19    |    0.13    |    0.38    |    0.16   |    0.15   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.16825983548466625.

Take your group selfies to another level with #GalaxyA2017.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.04    |    0.75    |    0.0    |    0.19   |
| predicted |     0.31    |    0.14    |    0.28    |    0.13   |    0.13   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2592059273588974.

Fitness, motivation & style now come together with Under Armour Connected Fitness Apps on the #GearFit2.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.03    |    0.76    |    0.01   |    0.19   |
| predicted |     0.29    |    0.18    |    0.25    |    0.16   |    0.12   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2749135077625223.

Go everywhere with a raincoat. #GalaxyA2017 #IP68
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.03    |    0.04    |    0.73    |    0.01   |    0.19   |
| predicted |     0.04    |    0.06    |    0.7     |    0.07   |    0.14   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.038991125731669206.

We<e2><80><99>ve identified the cause of the Galaxy Note7 cases. Learn about the steps we<e2><80><99>re taking moving forward. http://spr.ly/PressConf123
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.03    |    0.1     |    0.63    |    0.02   |    0.22   |
| predicted |     0.22    |    0.19    |    0.24    |    0.15   |    0.21   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2051777231905009.

Designer Arik Levy builds a beautiful bridge between art and technology with unique designs for the #GearS3.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.02    |    0.61    |    0.02   |    0.33   |
| predicted |     0.17    |    0.15    |    0.29    |    0.13   |    0.26   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.1787837133762553.

Big news is on its way. Don<e2><80><99>t miss out. http://spr.ly/61868rmse
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.03    |    0.53    |    0.0    |    0.44   |
| predicted |     0.25    |    0.14    |    0.3     |    0.19   |    0.12   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.22629478366660932.

We<e2><80><99>ve got big news. Tune in:
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.03    |    0.64    |    0.01   |    0.32   |
| predicted |     0.18    |    0.18    |    0.37    |    0.14   |    0.14   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.1876289497176674.

Our press conference is starting now: spr.ly/samsunglivestream
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.03    |    0.03    |    0.71    |    0.01   |    0.21   |
| predicted |     0.34    |    0.13    |    0.33    |    0.12   |    0.07   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.23932555957610332.

Introducing the all-new versatile Samsung #GalaxyTabS3. spr.ly/GalaxyTabS3
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.01    |    0.67    |    0.0    |    0.3    |
| predicted |     0.47    |    0.11    |    0.02    |    0.26   |    0.15   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.38036938902305.

The power and productivity of Windows 10 on a 2-in-1 device. Introducing the Samsung #GalaxyBook.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.01    |    0.64    |    0.0    |    0.33   |
| predicted |     0.26    |    0.09    |    0.28    |    0.18   |    0.19   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.22226331921847242.

This is what a phone looks like... until 03.29.2017. #UnboxYourPhone spr.ly/GalaxyUnpacked
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.06    |    0.64    |    0.01   |    0.27   |
| predicted |     0.26    |    0.19    |    0.22    |    0.17   |    0.16   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2387364061936679.

Take these Under Armour Connected Fitness apps for a run. #GearS3 #GearFit2 #GearS2
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.03    |    0.03    |    0.76    |    0.0    |    0.17   |
| predicted |     0.3     |    0.17    |    0.28    |    0.11   |    0.14   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2625072921349393.

Enjoy true-to-life, cinema-quality HDR movies (like you were at the movies). #GalaxyTabS3
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.02    |    0.74    |    0.01   |    0.21   |
| predicted |     0.16    |    0.13    |    0.44    |    0.18   |    0.09   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.18237433937010059.

Gaming-optimized graphics so realistic, you can taste the enemy<e2><80><99>s fear. #GalaxyTabS3
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.03    |    0.72    |    0.0    |    0.23   |
| predicted |     0.27    |    0.26    |    0.15    |    0.24   |    0.08   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.3190885202939977.

Phones will never be the same. 03.29.2017 #UnboxYourPhone spr.ly/GalaxyUnpacked
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.01    |    0.73    |    0.0    |    0.25   |
| predicted |     0.16    |    0.11    |    0.35    |    0.26   |    0.11   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.22794703194404514.

Free time just got a lot freer. The new #GalaxyTabS3 is here. spr.ly/GalaxyTabS3
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.01    |    0.64    |    0.0    |    0.33   |
| predicted |     0.27    |    0.17    |    0.29    |    0.13   |    0.13   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.23266159660560157.

We spent years crafting the Infinity Display from concept to final design. #UnboxYourPhone #MilanDesignWeek
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.03    |    0.06    |    0.72    |    0.01   |    0.18   |
| predicted |     0.15    |    0.29    |    0.27    |    0.11   |    0.18   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2335168652458048.

Go inside the design. Take a 360 tour of the #GalaxyS8 Studio built by Zaha Hadid Architects at #MilanDesignWeek. #UnboxYourPhone
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.01    |    0.6     |    0.01   |    0.36   |
| predicted |     0.19    |    0.14    |    0.39    |    0.09   |    0.19   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.1599093376479195.

Keep track of the news, while tracking every mile you run. Easily do it all with big updates to the #GearS3. http://spr.ly/GearS3ValPack
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.01    |    0.86    |    0.01   |    0.1    |
| predicted |     0.21    |    0.18    |    0.33    |    0.13   |    0.15   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2689337957256836.

At Unpacked on March 29, we illuminated NYC's Times Square with an immersive visual experience inspired by the #GalaxyS8. #UnboxYourPhone
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.01    |    0.62    |    0.0    |    0.36   |
| predicted |     0.23    |    0.06    |    0.41    |    0.2    |    0.11   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.199905413173283.

Grainy selfies no more. The #GalaxyS8 front camera gives you a crisp, clear image on the first shot. #UnboxYourPhone
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.01    |    0.77    |    0.01   |    0.2    |
| predicted |     0.23    |    0.11    |    0.32    |    0.16   |    0.19   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.23552392374915115.

Now each key is where you need it to be. Customize the layout of the #GalaxyS8's navigation bar. #UnboxYourPhone
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.02    |    0.73    |    0.01   |    0.22   |
| predicted |     0.15    |    0.26    |    0.32    |    0.08   |    0.19   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2206897766408423.

With Pressure Sensor on the #GalaxyS8, the new navigation keys do what your old home key used to do. #UnboxYourPhone
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.03    |    0.77    |    0.0    |    0.19   |
| predicted |     0.25    |    0.17    |    0.22    |    0.19   |    0.16   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2858055649015018.

Navigate your virtual experience more easily with the Controller for the new #GearVR
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.02    |    0.64    |    0.0    |    0.33   |
| predicted |     0.22    |    0.26    |    0.24    |    0.11   |    0.16   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.24367151280617516.

When you unbox your phone, you unbox your world. #UnboxYourPhone #GalaxyS8
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.02    |    0.74    |    0.01   |    0.23   |
| predicted |     0.29    |    0.13    |    0.22    |    0.16   |    0.21   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.27776995177072755.

When you unbox your phone, you unbox your entire universe. #UnboxYourPhone #GalaxyS8
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.02    |    0.75    |    0.01   |    0.21   |
| predicted |     0.32    |    0.15    |    0.24    |    0.11   |    0.18   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.275128250571275.

When your phone doesn't box you in, you can soar to great heights. #UnboxYourPhone #GalaxyS8
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.02    |    0.75    |    0.01   |    0.2    |
| predicted |     0.29    |    0.16    |    0.2     |    0.15   |    0.2    |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.29003717364990256.

When you dare to defy barriers, you set yourself free. So we've set you free from the confinements of a regular phone, so you can live an unconfined life. #UnboxYourPhone #GalaxyS8
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.02    |    0.74    |    0.0    |    0.23   |
| predicted |     0.25    |    0.2     |    0.25    |    0.15   |    0.16   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.26704741698444645.

The outdoors are about to get a lot greater. See the ultimate survivor Bear Grylls put one of the new Gear S3 watch faces to the test.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.22    |    0.61    |    0.0    |    0.17   |
| predicted |     0.23    |    0.1     |    0.34    |    0.2    |    0.13   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.18769563282630686.

With the #Gear360, see what else happened at your birthday dinner.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.62    |    0.27    |    0.01   |    0.08   |
| predicted |     0.17    |    0.16    |    0.4     |    0.18   |    0.08   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.23884729096773674.

Sleek to look at. Simple to use. Introducing the Samsung Wireless Charger Convertible.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.03    |    0.62    |    0.0    |    0.33   |
| predicted |     0.22    |    0.23    |    0.22    |    0.2    |    0.13   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.25576410743609634.

Introducing the new Samsung Battery Pack. It comes with a strap, so that you<e2><80><99>re never tied down.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.03    |    0.05    |    0.69    |    0.01   |    0.23   |
| predicted |     0.23    |    0.22    |    0.16    |    0.17   |    0.23   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.27633339915532534.

Look beyond your display and see the world unframed. Take control of a whole new dimension with the #GearVR and Controller.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.03    |    0.61    |    0.01   |    0.35   |
| predicted |     0.21    |    0.17    |    0.33    |    0.14   |    0.15   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.19686012399822422.

Give your phone more office space. #SamsungDeX easily hooks up your #GalaxyS8 to a bigger screen. #UnboxYourPhone
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.06    |    0.58    |    0.01   |    0.34   |
| predicted |     0.15    |    0.15    |    0.32    |    0.11   |    0.27   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.15346972590975355.

Keep your memories crystal-clear, no matter what side of the camera you<e2><80><99>re on. #GalaxyS8 #UnboxYourPhone
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.06    |    0.74    |    0.01   |    0.18   |
| predicted |     0.32    |    0.13    |    0.27    |    0.15   |    0.12   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2609048905046618.

Capture immersive views of the world in 4K with the #Gear360. Share your most memorable moments unframed.
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.0     |    0.02    |    0.67    |    0.01   |    0.3    |
| predicted |     0.16    |    0.22    |    0.33    |    0.11   |    0.17   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.20515620980688928.

Throw on a pump-up playlist while tracking your workout. Play Spotify right from your wrist with the #GearS3
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.02    |    0.8     |    0.01   |    0.15   |
| predicted |     0.23    |    0.1     |    0.24    |    0.31   |    0.13   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.29747813584844596.

Ready to experience a whole new level of taking notes? #GalaxyTabS3
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.08    |    0.62    |    0.01   |    0.3    |
| predicted |     0.23    |    0.17    |    0.38    |    0.1    |    0.12   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.1729498775285362.

Questions about your new Galaxy S8? Just ask. spr.ly/SamsungBot
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.05    |    0.2     |    0.67    |    0.01   |    0.08   |
| predicted |     0.14    |    0.21    |    0.21    |    0.16   |    0.27   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.2347078961533752.

Want to stream tunes on your new Galaxy S8? Just ask. spr.ly/S8Music
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.01    |    0.3     |    0.59    |    0.01   |    0.09   |
| predicted |     0.14    |    0.19    |    0.2     |    0.28   |    0.19   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.22686163659638275.

Power saving keeps your Galaxy S8 juiced. Want to know how? Just ask. spr.ly/S8Battery
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.04    |    0.08    |    0.66    |    0.01   |    0.2    |
| predicted |     0.13    |    0.17    |    0.31    |    0.18   |    0.2    |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.18284225255091288.

Paying with your new Galaxy S8 is easy. Want to know how? Just ask. spr.ly/S8Pay
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.07    |    0.12    |    0.57    |    0.04   |    0.19   |
| predicted |     0.2     |    0.22    |    0.23    |    0.16   |    0.19   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.17868955540005046.

Want to make a video call on your new Galaxy S8? Just ask. spr.ly/S8VideoCall
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.05    |    0.11    |    0.61    |    0.02   |    0.23   |
| predicted |     0.22    |    0.18    |    0.25    |    0.17   |    0.19   |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.1954898417539045.

Your music may be bad. But it<e2><80><99>s yours. Switch to Galaxy, easily keep your music. spr.ly/S8SmartSwitch
+-----------+-------------+------------+------------+-----------+-----------+
|   values  | angry_count | haha_count | love_count | sad_count | wow_count |
+-----------+-------------+------------+------------+-----------+-----------+
|   actual  |     0.02    |    0.16    |    0.57    |    0.01   |    0.24   |
| predicted |     0.15    |    0.2     |    0.27    |    0.18   |    0.2    |
+-----------+-------------+------------+------------+-----------+-----------+
RMSE is 0.16522419588864964.


Mean RMSE is 0.229